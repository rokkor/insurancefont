import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { InsureService } from './insure.service';
import { KybackModule } from '@kykub/back';
import { Configfile } from '@kykub/base';

let cfg: Configfile = {
  host: 'http://localhost:8080', urladd: '/insurance/add'
}
describe('InsureService', () => {
  let service: InsureService;
  let httpMock: HttpTestingController;
  let i = { name: 'test' }
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, KybackModule],
      providers: [{ provide: 'InCONFIG', useValue: cfg }, InsureService
      ]
    });
    httpMock = TestBed.inject(HttpTestingController)
    service = TestBed.inject(InsureService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('Test add ', () => {
    const mockCourse = {
      name: 'Chessable',
      description: 'Space repetition to learn chess, backed by science'
    };
    service.add(i).subscribe((d:any)=>{
      expect(d.name).toEqual('Chessable');
    })
    const req = httpMock.expectOne('http://localhost:8080/insurance/add');
    req.flush(mockCourse);
  })
});
