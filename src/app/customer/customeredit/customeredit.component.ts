import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customeredit',
  templateUrl: './customeredit.component.html',
  styleUrls: ['./customeredit.component.css']
})
export class CustomereditComponent implements OnInit {

  constructor(public cs: CustomerService, public router: Router, private route: ActivatedRoute, public bar: MatSnackBar) { }
  id = 0
  customer = { name: '', id: 0, tel: '', ver: 0, description: '' }
  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      console.log('Qurey', params)
      console.log("id", this.id)
      this.cs.get(this.id).subscribe((d: any) => {
        console.log('Customer', d)

        this.customer = d
      })
    });
  }

  edit() {
    this.cs.edit(this.customer).subscribe((editcustomer: any) => {
      this.customer = editcustomer
      this.bar.open('Edit', JSON.stringify(editcustomer), { duration: 2000 })
    }, e => {
      this.bar.open('Can not edit', e.message, { duration: 5000 })
    })
  }

}
