import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customeradd',
  templateUrl: './customeradd.component.html',
  styleUrls: ['./customeradd.component.css']
})
export class CustomeraddComponent implements OnInit {
  name: any
  tel: any
  rows = Array<any>()
  constructor(public cs: CustomerService) { }

  ngOnInit(): void {
    this.update()
  }
  update() {
    this.cs.sn({ search: '', page: 0, limit: 1000 }).subscribe((d: any) => {
      this.rows = d
    })
  }
  add() {
    this.cs.add({ name: this.name, tel: this.tel }).subscribe(d => {
        console.log('Add',d)
        this.name  = ""
        this.tel = ""
        this.update()
    })
  }
}
