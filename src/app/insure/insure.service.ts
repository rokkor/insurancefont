import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Configfile, Kybase6Service } from '@kykub/base';
import { Cfg } from '../cfg';

@Injectable()
export class InsureService extends Kybase6Service {
  constructor(h: HttpClient,  @Inject('InCONFIG')  c: Configfile) {
    console.log('C in Insurance',c)

    super(h, c)
    this.config.urladd='/insurance/add'
    this.config.urlsn ='/insurance/sn'
  }
  addType(ty:any)
  {
    let url = this.config.host+"/insurance/addtype"
    return this.http.post(url,ty)
  }
}
