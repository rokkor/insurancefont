import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InsuretypeaddComponent } from './insuretypeadd/insuretypeadd.component';
import { MatModule } from '../mat/mat.module';
import { Configfile } from '@kykub/base';
import { InsureService } from './insure.service';
import { HttpClientModule } from '@angular/common/http';
import { InsurancetypeService } from './insurancetype.service';
import { InsuranceaddComponent } from './insuranceadd/insuranceadd.component';
import { AutoModule } from '@kykub/auto';
import { InsurancetypeeditComponent } from './insurancetypeedit/insurancetypeedit.component';



@NgModule({
  declarations: [
    InsuretypeaddComponent,
    InsuranceaddComponent,
    InsurancetypeeditComponent
  ],
  imports: [
    CommonModule, MatModule, AutoModule
  ]
})
export class InsureModule {
  static forRoot(cfg: Configfile): ModuleWithProviders<InsureModule> {
    return {
      ngModule: InsureModule,
      providers: [
        { provide: 'InCONFIG', useValue: cfg },
        InsureService,
        InsurancetypeService
      ]
    }
  }
}
