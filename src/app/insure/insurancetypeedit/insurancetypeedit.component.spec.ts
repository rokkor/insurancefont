import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsurancetypeeditComponent } from './insurancetypeedit.component';

describe('InsurancetypeeditComponent', () => {
  let component: InsurancetypeeditComponent;
  let fixture: ComponentFixture<InsurancetypeeditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsurancetypeeditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsurancetypeeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
