import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Configfile, Kybase6Service } from '@kykub/base';

@Injectable({
  providedIn: 'root'
})
export class CustomerService extends Kybase6Service {

  constructor(h: HttpClient, @Inject('CUSTOMERCONFIG') c: Configfile) { super(h, c) }
}
