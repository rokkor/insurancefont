import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatModule } from './mat/mat.module';
import { KybackModule } from "@kykub/back";
import { InsureModule } from './insure/insure.module';
import { HttpClientModule } from '@angular/common/http';
import { CustomerModule } from './customer/customer.module';
import { AutoModule } from '@kykub/auto';
import { environment } from "src/environments/environment";
import { SavedataService } from './savedata.service';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatModule,
    KybackModule,
    AutoModule,
    InsureModule.forRoot({ host: environment.host, urladd: '/insurance/add', urlsn: '/insurance/sn' }),
    CustomerModule.forRoot({ host: environment.host, urladd: '/customer/add', urlsn: '/customer/sn',
     urlget: '/customer/get',urledit:'/customer/edit' })
  ],
  providers: [SavedataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
