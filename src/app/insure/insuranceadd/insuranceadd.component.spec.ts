import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuranceaddComponent } from './insuranceadd.component';

describe('InsuranceaddComponent', () => {
  let component: InsuranceaddComponent;
  let fixture: ComponentFixture<InsuranceaddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsuranceaddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
