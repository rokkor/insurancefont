import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Configfile } from '@kykub/base';
import { CustomeraddComponent } from './customeradd/customeradd.component'
import { CustomerService } from './customer.service';
import { MatModule } from '../mat/mat.module';
import { CustomereditComponent } from './customeredit/customeredit.component';



@NgModule({
  declarations: [
    CustomeraddComponent,
    CustomereditComponent
  ],
  imports: [
    CommonModule, MatModule
  ]
})
export class CustomerModule {
  static forRoot(cfg: Configfile): ModuleWithProviders<CustomerModule> {
    return {
      ngModule: CustomerModule,
      providers: [
        { provide: 'CUSTOMERCONFIG', useValue: cfg },
        CustomerService
      ]
    }
  }
}


