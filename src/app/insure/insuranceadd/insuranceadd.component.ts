import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/customer/customer.service';
import { InsurancetypeService } from '../insurancetype.service';
import { InsureService } from '../insure.service';
import { environment } from "src/environments/environment";
@Component({
  selector: 'app-insuranceadd',
  templateUrl: './insuranceadd.component.html',
  styleUrls: ['./insuranceadd.component.css']
})
export class InsuranceaddComponent implements OnInit {
  value: any
  rows: Array<any> = []
  basepay = 0
  secoundpay = 0
  css = { obj: { id: 0, name: '' } }
  it = { obj: { id: 0, name: '' } }
  constructor(public iss: InsureService, public cs: CustomerService, public itys: InsurancetypeService) { }

  ngOnInit(): void {
    this.update()
  }

  add() {

    let o = {
      customer: this.css.obj,
      insureruntype: this.it.obj,
      pay2: this.secoundpay,
      pay1: this.basepay,
    }
    this.iss.add(o).subscribe(d => {
      console.log('Add', o)
      this.css.obj = { id: 0, name: '' }
      this.it.obj = { id: 0, name: '' }
      this.basepay = 0
      this.secoundpay = 0
      this.update()
    })
    console.log('Data ', o)

  }

  update() {
    this.iss.sn({ search: '', page: 0, limit: 1000 }).subscribe((d: any) => {
      this.rows = d
      console.log('Rows ', d)
    })
  }
}
