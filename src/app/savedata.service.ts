import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SavedataService {
  save(n:string,d:any)
  {
    localStorage.setItem(n,JSON.stringify(d))
  }

  load(n:string)
  {
      let o = localStorage.getItem(n)
      if(o!=null)
       return JSON.parse(o)

       return null
  }

  constructor() { }
}
