import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomeraddComponent } from './customer/customeradd/customeradd.component';
import { CustomereditComponent } from './customer/customeredit/customeredit.component';
import { InsuranceaddComponent } from './insure/insuranceadd/insuranceadd.component';
import { InsuretypeaddComponent } from './insure/insuretypeadd/insuretypeadd.component';

const routes: Routes = [
  { path: 'insuretypenew', component: InsuretypeaddComponent, },
  { path: 'customernew', component: CustomeraddComponent, },
  { path: 'customeredit', component: CustomereditComponent, },
  { path: 'insurance', component: InsuranceaddComponent, },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
