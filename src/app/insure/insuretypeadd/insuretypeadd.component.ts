import { Component, OnInit } from '@angular/core';
import { InsurancetypeService } from '../insurancetype.service';
import { InsureService } from '../insure.service';

@Component({
  selector: 'app-insuretypeadd',
  templateUrl: './insuretypeadd.component.html',
  styleUrls: ['./insuretypeadd.component.css']
})
export class InsuretypeaddComponent implements OnInit {

  value = ""
  com = 0
  added: any //for test
  rows = Array<any>()
  constructor(public iss: InsurancetypeService, public ins: InsureService) { }

  add() {
    console.log('Data in service', this.iss)
    this.iss.add({ name: this.value, comp: this.com }).subscribe((d: any) => {
      console.log('Add ok', d)
      this.added = d
      this.value = ""
      this.com = 0
      this.update();
    })
  }
  update() {
    this.iss.sn({ search: '', page: 0, limit: 1000 }).subscribe((d: any) => {

      this.rows = d
      console.log('Rows ', this.rows)
    })
  }
  ngOnInit(): void {
    console.log('Ins ', this.ins)
    this.update()
  }

}
