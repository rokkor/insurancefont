import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Configfile } from '@kykub/base';
import { InsureService } from '../insure.service';

import { InsuretypeaddComponent } from './insuretypeadd.component';

describe('InsuretypeaddComponent', () => {
  let component: InsuretypeaddComponent;
  let fixture: ComponentFixture<InsuretypeaddComponent>;
  let cfg: Configfile = { host: 'http://localhost:8080', urladd: '/insurance/add', urlsn: '/insurance/sn' }
  let service: InsureService;
  let httpMock: HttpTestingController;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: 'InCONFIG', useValue: cfg }, InsureService],
      declarations: [InsuretypeaddComponent]

    })
      .compileComponents();


  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuretypeaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    httpMock = TestBed.inject(HttpTestingController)
    service = TestBed.inject(InsureService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test add function', () => {
    const o = { name: 'aaaa' }
    component.value = "name"
    component.com = 10
    component.add()
    const req = httpMock.expectOne('http://localhost:8080/insurance/add');
    req.flush(o);
    // console.log('Add ed', component.added)
    expect(component.added.name).toEqual('aaaa');
  })

  it('test list insurance data in ', () => {
    const rows = [
      { name: '' }, { name: '' }
    ]
    component.update()
    const req = httpMock.expectOne('http://localhost:8080/insurance/sn');
    req.flush(rows);
    expect(component.rows.length).toEqual(2);

  })
});
