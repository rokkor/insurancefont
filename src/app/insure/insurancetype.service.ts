import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Configfile, Kybase6Service } from '@kykub/base';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InsurancetypeService extends Kybase6Service {

  constructor(h: HttpClient, @Inject('InCONFIG') c: Configfile) {
    super(h, c)
    console.log('C in type',c)
  
    this.urladd = '/insurancetype/add'
    this.urlsn = '/insurancetype/list'
  }

 
}
