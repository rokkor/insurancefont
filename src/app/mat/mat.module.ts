import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [
    CommonModule, MatButtonModule, MatInputModule, MatIconModule,
    MatMenuModule, MatFormFieldModule,FormsModule
  ]
  , exports: [MatButtonModule, MatInputModule,
     MatIconModule,MatMenuModule, MatFormFieldModule,FormsModule]
})
export class MatModule { }
